""" Module d'appel d'une API """

import urllib.parse as urlparse
from urllib.parse import urlencode
import requests

def get_api(url):
    """Fonction d'appel à l'API"""
    try:
        response = requests.get(url, timeout=10)
    except requests.exceptions.Timeout as error:
        # Maybe set up for a retry, or continue in a retry loop
        print('ERREUR', error)
        return False
    except requests.exceptions.TooManyRedirects as error:
        # Tell the user their URL was bad and try a different one
        print('ERREUR', error)
        return False
    except requests.exceptions.RequestException as error:
        # catastrophic error. bail.
        print('ERREUR', error)
        return False
    return response.json()

def url_param(url, params):
    """Construction d'une url avec des parametres"""

    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    url_complete = urlparse.urlunparse(url_parts)

    return url_complete
