#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Paramétrage des url et des fichiers
"""

URL_STATIONS = "https://velib-metropole-opendata.smovengo.cloud/opendata/Velib_Metropole/station_information.json"

URL_STATIONS_STATUS = "https://velib-metropole-opendata.smovengo.cloud/opendata/Velib_Metropole/station_status.json"

URL_ADRESS = "https://api-adresse.data.gouv.fr/reverse/"

PATH_DATA_STATIONS = "./app/data/data_stations.json"
