#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Recuperer le status d'un station
"""
from settings import URL_STATIONS_STATUS
from api_utils import get_api

def station_status(station_id):
    """recuperer les informations de
    disponibilité des vélos d'une station"""
    status = get_api(URL_STATIONS_STATUS)
    for stations in status["data"]["stations"]:
#        print(stations)
        if stations["station_id"] == station_id:
            return stations
    return None

print(station_status(54000572))
