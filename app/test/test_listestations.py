from unittest import TestCase

class TestStationVelib(TestCase):
    def test_listestations(self):
        from listestations import listestations
        self.assertIn("213688169", str(listestations()), "OK")

    def test_liste_villes(self):
        from listestations import liste_villes
        self.assertIn("75001 Paris", liste_villes(), "OK")

if __name__ == "__main__":
    unittest.main()
