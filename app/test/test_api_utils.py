from unittest import TestCase

class TestApiUtils(TestCase):
    def test_url_param(self):
        from api_utils import url_param
        params = {'lon':0.0,'lat':0.0,'type':'street'}
        url_address = "https://api-adresse.data.gouv.fr/reverse/"
        url_param = url_param(url_address, params)
        self.assertEqual(url_param, "https://api-adresse.data.gouv.fr/reverse/?lon=0.0&lat=0.0&type=street")

if __name__ == "__main__":
    unittest.main()
