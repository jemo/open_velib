from unittest import TestCase

class TestStationStatus(TestCase):
    def test_station_status(self):
        from station_status import station_status
        self.assertEqual(34, station_status(213688169))

if __name__ == "__main__":
    unittest.main()
