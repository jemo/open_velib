from unittest import TestCase

class TestRefreshStations(TestCase):
    def test_refresh_stations(self):
        from refresh_stations import refresh_stations
        from settings import path_data_stations
        import os

        refresh_stations()
        self.assertTrue(os.path.isfile(path_data_stations))

    def test_get_address(self):
        from refresh_stations import get_address

        rue, code_postal = get_address(2.37,48.357)
        self.assertEqual(rue, "Chemin de Pithiviers")
        self.assertEqual(code_postal, "91720 Prunay-sur-Essonne")

if __name__ == "__main__":
    unittest.main()
