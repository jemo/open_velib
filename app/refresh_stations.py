#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Rafraichir le fichier de la liste des stations
"""
import json

from settings import URL_STATIONS, PATH_DATA_STATIONS
from api_utils import get_api

def refresh_stations():
    """recuperer et mettre en forme la liste des stations
        les classer par code postal"""
    resultats = get_api(URL_STATIONS)
    velib = []
    for station in resultats["data"]["stations"]:
        velib.append(station)
    with open(PATH_DATA_STATIONS, 'w', encoding="utf-8") as file:
        json.dump(velib, file, indent=2)

refresh_stations()
