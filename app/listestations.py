""" Module pour lister les stations"""
import json
from settings import PATH_DATA_STATIONS

def listestations():
    """recuperer la liste des stations"""
    with open(PATH_DATA_STATIONS, encoding="utf-8") as f:
        resultats = json.load(f)
        for stations in resultats:
            print(stations[]["lat"])
#    print (resultats)
    return resultats

#def liste_villes():
#    """Liste des villes"""
#    stations = listestations()
#    cp_ville = []
#    for station, detail in stations.items():
#        cp_ville.append(detail["cp_ville"])
#
#    cp_ville_sort = list(set(cp_ville))
#    cp_ville_sort.sort()
#    return cp_ville_sort

listestations()
