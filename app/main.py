""" Module principale"""

from kivymd.app import MDApp
from openvelibmap import OpenVelibMap
from refresh_stations import refresh_stations

class MainApp(MDApp):
    """Class principale de l'application"""

    def on_start(self):
        refresh_stations()

if __name__ == '__main__':
    MainApp().run()
