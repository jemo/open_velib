# App pour avoir la disponibilité des Vélib'

Cette application permet de consulter les stations Vélib' par arrondissements, et de connaître les vélos disponible dans chaques stations.

## Fonctionnel
### APP
* liste des stations

   Prevoir un filtre par arrondissements

* stations favorites

* Fonction pour rafraichir les données

   Le fichier data_stations.json doit contenir le détail pour les stations
   * Le nom

### VELIB 
[API pour Vélib'](https://www.velib-metropole.fr/donnees-open-data-gbfs-du-service-velib-metropole)

[API retrouver adresse, CP](https://geo.api.gouv.fr/adresse)

## Technique
[Utilisation API avec Python](https://www.digitalocean.com/community/tutorials/how-to-use-web-apis-in-python-3)

[Request api python](https://stackoverflow.com/questions/6386308/http-requests-and-json-parsing-in-python)

[How to dynamically build a JSON object with Python?](https://stackoverflow.com/questions/23110383/how-to-dynamically-build-a-json-object-with-python)

### [Kivy](https://kivy.org/)
[Material design for Kivy](https://kivymd.readthedocs.io/en/latest/)

[exemple app kivy md](https://github.com/adityabhawsingka/ExpenseTracker)

[Packaging pour Android](https://kivy.org/doc/stable/guide/packaging-android.html)

### [Unittest](https://docs.python.org/3/library/unittest.html)

Pour lancer les tests 

`cd app && python3 -m unittest`

### Cheatsheet
[Venv cheatsheet](https://framagit.org/jemo/notes/-/blob/main/Programmation/Modules/venv.md)

[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
